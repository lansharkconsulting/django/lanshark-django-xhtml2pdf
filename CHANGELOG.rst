Changelog
---------

- 0.0.8
   * TBD

- 0.0.7
   * clean up reference to parent project
   * make compatible with Python 3.6

- 0.0.6
   * make compatible with Django 2.0
   * make compatible with Python 3.8
